<?php

/**
 * Use this file to output reports required for the SQL Query Design test.
 * An example is provided below. You can use the `asTable` method to pass your query result to,
 * to output it as a styled HTML table.
 */

//$database = 'nba2019';
require_once('vendor/autoload.php');
require_once('include/utils.php');

/*
 * Example Query
 * -------------
 * Retrieve all team codes & names
 */
echo '<h1>Example Query</h1>';
$teamSql = "SELECT * FROM team";
$teamResult = query($teamSql);
// dd($teamResult);
echo asTable($teamResult);

/*
 * Report 1
 * --------
 * Produce a query that reports on the best 3pt shooters in the database that are older than 30 years old. Only 
 * retrieve data for players who have shot 3-pointers at greater accuracy than 35%.
 * 
 * Retrieve
 *  - Player name
 *  - Full team name
 *  - Age
 *  - Player number
 *  - Position
 *  - 3-pointers made %
 *  - Number of 3-pointers made 
 *
 * Rank the data by the players with the best % accuracy first.
 */
echo '<h1>Report 1 - Best 3pt Shooters</h1>';
// write your query here
$tpsSql = "SELECT
	r.name AS 'Player name',
	t.name AS 'Team name',
	pt.age AS 'AGE',
	r.number AS 'Player number',
	r.pos AS 'Position',
	ROUND(pt.3pt / pt.3pt_attempted * 100, 0) AS '3-pointers made %',
	pt.3pt AS '3-pointers made'
FROM roster r
INNER JOIN team t ON r.team_code=t.code
INNER JOIN player_totals pt ON r.id=pt.player_id
WHERE pt.age > 30 AND ROUND(pt.3pt / pt.3pt_attempted * 100, 0) > 35
ORDER BY ROUND(pt.3pt / pt.3pt_attempted * 100, 0) DESC";
$tpsResult = query($tpsSql);
echo asTable($tpsResult);
/*
 * Report 2
 * --------
 * Produce a query that reports on the best 3pt shooting teams. Retrieve all teams in the database and list:
 *  - Team name
 *  - 3-pointer accuracy (as 2 decimal place percentage - e.g. 33.53%) for the team as a whole,
 *  - Total 3-pointers made by the team
 *  - # of contributing players - players that scored at least 1 x 3-pointer
 *  - of attempting player - players that attempted at least 1 x 3-point shot
 *  - total # of 3-point attempts made by players who failed to make a single 3-point shot.
 * 
 * You should be able to retrieve all data in a single query, without subqueries.
 * Put the most accurate 3pt teams first.
 */
echo '<h1>Report 2 - Best 3pt Shooting Teams</h1>';
// write your query here
$tpstSql = "SELECT 
    t.name AS 'Team name',
    ROUND(SUM(pt.3pt) / SUM(pt.3pt_attempted) * 100, 2) AS '3-pointer accuracy',
    SUM(pt.3pt) AS '3-pointers made',
    SUM(case when pt.3pt <> 0 then 1 else 0 end) AS '# of contributing players',
    SUM(case when pt.3pt_attempted <> 0 then 1 else 0 end) AS '# of attempting players',
    SUM(case when pt.3pt = 0 AND pt.3pt_attempted <> 0 then 1 else 0 end) AS '# of failed attempting players'
FROM roster r
INNER JOIN team t ON r.team_code=t.code
INNER JOIN player_totals pt ON r.id=pt.player_id
GROUP BY t.name";
$tpstResult = query($tpstSql);
echo asTable($tpstResult);
?>